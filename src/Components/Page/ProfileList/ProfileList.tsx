import React, { Fragment, useState, useEffect } from "react";
import {
  Form,
  Input,
  Button,
  Drawer,
  Checkbox,
  Row,
  Col,
  Select,
  Space,
  Typography,
  Layout,
  Menu,
} from "antd";
import { FilterOutlined, EnvironmentOutlined } from "@ant-design/icons";
import "./ProfileList.css";
import accion from "../../../Assets/linkedin/logo.png";
const { Title } = Typography;
const { Header } = Layout;
const { Option } = Select;

const { Search } = Input;

const locations = [
  'New York',
  'Los Angeles',
  'Chicago',
  'Houston',
  'Phoenix',
  'Philadelphia',
  'San Antonio',
  'San Diego',
  'Dallas',
  'San Jose',
];

const filters = [
  { label: 'North America', value: 'north-america' },
  { label: 'Europe', value: 'europe' },
  { label: 'Asia', value: 'asia' },
  { label: 'Australia', value: 'australia' },
  { label: 'Africa', value: 'africa' },
  { label: 'South America', value: 'south-america' },
];
const ProfileList: React.FC = () => {
  const [form] = Form.useForm();
  const [visible, setVisible] = useState(false);
  const [filterType, setFilterType] = useState<string | null>(null);
  const [currentProfile, setCurrentProfile] = useState<number | null>(null);

  const [drawerVisible, setDrawerVisible] = useState(false);
  const [selectedLocation, setSelectedLocation] = useState<string | undefined>(undefined);
  const [filteredLocations, setFilteredLocations] = useState(locations);
  const [checkedFilters, setCheckedFilters] = useState<string[]>([]);

  const showDrawer = (type: string, profileIndex: number) => {
    setFilterType(type);
    setCurrentProfile(profileIndex);
    setVisible(true);
  };
  const showDrawer1 = () => {
    setDrawerVisible(true);
  };

  const onClose = () => {
    setVisible(false);
    setFilterType(null);
    setCurrentProfile(null);
    setDrawerVisible(false);
  };

  const handleReset = () => {
    form.resetFields();
  };

  const handleFinish = (values: any) => {
    console.log("Form values:", values);
  };

  const handleFilterSave = () => {
    console.log(
      `Filter options saved for ${filterType} in profile ${currentProfile}`
    );
    onClose();
  };

  const handleLocationChange = (value: string) => {
    setSelectedLocation(value);
  };

  const handleSearch = (value: string) => {
    const searchValue = value.toLowerCase();
    const filtered = locations.filter((location) =>
      location.toLowerCase().includes(searchValue)
    );
    setFilteredLocations(filtered);
  };

  const handleFilterChange = (checkedValues: any) => {
    setCheckedFilters(checkedValues);
  };
  const [inputValue, setInputValue] = useState('');

  const handleInputChange = (e:any) => {
    setInputValue(e.target.value);
  };

  return (
    <Fragment>
      <Header
        style={{
          position: "sticky",
          top: 0,
          zIndex: 1,
          background: "#e0e0e0",
          maxWidth: "100%",
        }}
      >
        <img
          src={accion}
          style={{
            float: "left",
            width: 150,
            height: 35,
            margin: "16px 44px 16px 0",
          }}
          alt="Accionlabs"
        />
        <Menu theme="dark" mode="horizontal" style={{ lineHeight: "64px" }} />
      </Header>
      <Form
        form={form}
        layout="vertical"
        onFinish={handleFinish}
        style={{ maxWidth: "800px", margin: "0 auto" }}
      >
        <Row gutter={16}>
          <Col span={6}>
            <div className="login-box">
              <Title level={4} className="login-header">
                Profile List
              </Title>
            </div>
          </Col>
          <Col span={6}>
            <Button
              style={{ marginTop: "45px" }}
              type="default"
              onClick={handleReset}
            >
              Reset
            </Button>
          </Col>
        </Row>
        {[...Array(5)].map((_, index) => (
          <Row gutter={16} key={index} style={{ marginBottom: "24px" }}>
            <Col span={6}>
              <Form.Item
                name={`profileName${index + 1}`}
                // rules={[
                //   {
                //     required: true,
                //     message: "Please input your profile name!",
                //   },
                // ]}
              >
                <Input value={inputValue}
        onChange={handleInputChange} placeholder={`Enter profile name ${index + 1}`} />
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item
                name={`location${index + 1}`}
                // rules={[
                //   { required: true, message: "Please select your location!" },
                // ]}
              >
                <Checkbox disabled={!!inputValue}>
                  {/* <Button
                    icon={<FilterOutlined />}
                    onClick={() => showDrawer("location", index + 1)}
                    type="text"
                  >
                    Location
                  </Button> */}
                  <div>
      <Button disabled={!!inputValue} type="text" icon={<FilterOutlined />} onClick={showDrawer1}>
        {selectedLocation ? selectedLocation : 'Select Location'}
      </Button>
      <Drawer title="Select a Location" placement="right" onClose={onClose} visible={drawerVisible}>
        <Search placeholder="Search location" onSearch={handleSearch} style={{ marginBottom: 8 }} />
        <Checkbox.Group options={filters} onChange={handleFilterChange} style={{ marginBottom: 16 }} />
        <Select
          style={{ width: '100%' }}
          placeholder="Select a city"
          onChange={handleLocationChange}
          value={selectedLocation}
        >
          {filteredLocations.map((location) => (
            <Option key={location} value={location}>
              {location}
            </Option>
          ))}
        </Select>
      </Drawer>
    </div>
                </Checkbox>
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item
                name={`department${index + 1}`}
                // rules={[
                //   { required: true, message: "Please select your department!" },
                // ]}
              >
                <Checkbox disabled={!!inputValue}>
                  <Button disabled={!!inputValue}
                    icon={<FilterOutlined />}
                    onClick={() => showDrawer("department", index + 1)}
                    type="text"
                  >
                    Department
                  </Button>
                </Checkbox>
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item
                name={`contact${index + 1}`}
                // rules={[
                //   {
                //     required: true,
                //     message: "Please input your contact information!",
                //   },
                // ]}
              >
                <Checkbox disabled={!!inputValue}>
                  <Button disabled={!!inputValue}
                    icon={<FilterOutlined />}
                    onClick={() => showDrawer("contact", index + 1)}
                    type="text"
                  >
                    Contact
                  </Button>
                </Checkbox>
              </Form.Item>
            </Col>
          </Row>
        ))}

        <Form.Item>
          <Button type="primary" htmlType="submit">
            Extract Data
          </Button>
        </Form.Item>
      </Form>

      <Drawer
        title={`Filter ${filterType}`}
        placement="right"
        onClose={onClose}
        visible={visible}
        footer={
          <div style={{ textAlign: "right" }}>
            <Button onClick={onClose} style={{ marginRight: 8 }}>
              Cancel
            </Button>
            <Button onClick={handleFilterSave} type="primary">
              Save
            </Button>
          </div>
        }
      >
        {filterType && (
          <Checkbox.Group style={{ width: "100%" }}>
            <Row>
              <Col span={24}>
                <Checkbox value="option1">Option 1</Checkbox>
              </Col>
              <Col span={24}>
                <Checkbox value="option2">Option 2</Checkbox>
              </Col>
              <Col span={24}>
                <Checkbox value="option3">Option 3</Checkbox>
              </Col>
            </Row>
          </Checkbox.Group>
        )}
      </Drawer>
    </Fragment>
  );
};
export default ProfileList;
