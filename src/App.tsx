import React, { Fragment } from "react";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import "./App.css";
import Login from "./Auth/Login/Login";
import ProfileList from "./Components/Page/ProfileList/ProfileList";
function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route index path="/" element={<Login />} />
          <Route path="/Login" element={<Login />} />
          <Route path="/ProfileList" element={<ProfileList />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}
export default App;