import React, { useState, useEffect } from "react";
import { Form, Input, Button, Checkbox, Typography, Card } from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
import "./Login.css";
import { useNavigate } from "react-router-dom";
import logo from "../../Assets/linkedin/logo.png";

const { Title } = Typography;
const Login: React.FC = () => {
  const navigate = useNavigate();
  const [otp, setOtp] = useState<string[]>(new Array(6).fill(""));
  const [isButtonDisabled, setButtonDisabled] = useState(false);
  const onFinish = (values: any) => {
    console.log("Received values of form: ", values);
    setButtonDisabled(true);
    alert("OTP Recieved!");
  };

  const handleChange = (
    e: React.ChangeEvent<HTMLInputElement>,
    index: number
  ) => {
    const { value } = e.target;
    if (/^\d$/.test(value) || value === "") {
      const newOtp = [...otp];
      newOtp[index] = value;
      setOtp(newOtp);

      if (value && index < 5) {
        const nextSibling = document.getElementById(`otp-${index + 1}`);
        if (nextSibling) {
          nextSibling.focus();
        }
      }
    }
  };
  const handleVerify = () => {
    const enteredOtp = otp.join("");
    setButtonDisabled(false);
    alert("Verified Successfully!");
    navigate("/ProfileList");
    console.log("Entered OTP:", enteredOtp);
  };

  return (
    <div className="login-container">
      <Card className="login-card">
        <Form
          name="normal_login"
          initialValues={{ remember: true }}
          onFinish={onFinish}
          // labelCol={{ span: 8 }}
          // wrapperCol={{ span: 16 }}
          colon={false}
          requiredMark={false}
        >
          <img src={logo} />
          <div className="login-box">
            <Title level={4} className="login-header">
              Login to access profile
            </Title>
          </div>
          <Form.Item
            // label="Username"
            name="email"
            rules={[{ required: true, message: "Please input your Username!" }]}
          >
            <Input
              prefix={<UserOutlined className="site-form-item-icon" />}
              type="text"
              placeholder="Type username"
            />
          </Form.Item>
          <Form.Item
            // label="Password"
            name="password"
            rules={[{ required: true, message: "Please input your Password!" }]}
          >
            <Input
              prefix={<LockOutlined className="site-form-item-icon" />}
              type="password"
              placeholder="Type Password"
            />
          </Form.Item>
          <Form.Item
          // wrapperCol={{ offset: 8, span: 16 }}
          >
            <Button
              className="login-form-button"
              type="primary"
              htmlType="submit"
              disabled={isButtonDisabled}
            >
              Submit
            </Button>
          </Form.Item>
          <div>
            <Typography.Text className="otp-instruction">
              An OTP has been sent to your email:
            </Typography.Text>
            <Form className="otp-form">
              <div className="otp-inputs">
                {otp.map((value, index) => (
                  <Input
                    key={index}
                    id={`otp-${index}`}
                    value={value}
                    onChange={(e) => handleChange(e, index)}
                    maxLength={1}
                    className="otp-input"
                  />
                ))}
              </div>
              <Form.Item>
                <Button
                  disabled={!isButtonDisabled}
                  type="primary"
                  onClick={handleVerify}
                  className="otp-form-button"
                >
                  Verify
                </Button>
              </Form.Item>
            </Form>
          </div>
        </Form>
      </Card>
    </div>
  );
};
export default Login;
